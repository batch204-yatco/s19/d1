// console.log("Hello World");

/*
	Selection Control Structures
		- sorts out whether statement/s are to be executed based on the condition whether true or false

	if else statement
	switch statement
	
	if-else statement

	Syntax:
		if(condition){
			//statement
		} else {
			//statement
		}

*/

// IF STATEMENT
	// executes statement if a specified condition is true 
	// can stand alone even without else statement
	/*
		syntax:
			if(condition){
				code block
			}
	*/ 

let numA = -1;

if (numA < 0){
	console.log("Hello");
}

console.log(numA < 0);

if(numA > 0) {
	console.log("This statement will not be printed");
}

console.log(numA > 0);

// Another Example
let city = "New York";

if (city === "New York"){
	console.log("Welcome to New York Ciy!");
}

// Else If
/*
	- Executes a statement if previous conditions are false and if specified condition is true
	- Else if is optional and can be added to capture additional conditions to change flow of program
*/

let numB = 1;

if(numA > 0){
	console.log("Hello from elseif");
} else if(numB > 0) {
	console.log("World");
}

// Another Example
city = "Tokyo";

if (city === "New York"){
	console.log("Welcome to New York City");
} else if (city === "Tokyo") {
	console.log("Welcome to Tokyo!");
}

// else statement 
/*
	- Executes a statement if all other conditions are false
	- Else statement is optional and can be added to capture any other result to change flow of program
*/

if (numA > 0){
	console.log("Hello");
} else if(numB === 0) {
	console.log("World");
} else {
	console.log("Again!");
}

// Another Example
// let age = 20;

// if(age <= 18){
// 	console.log("Not allowed to drink");
// } else {
// 	console.log("Matanda ka na, shot na!");
// }

// Both printed
if (numA === -1){
	console.log("Hello IF");
}

if (numB === 1){
	console.log("World IF");
}

/*
	Mini-Activity:
		Create a conditional statement that if height is below 150, display "Did not pass the minimum height requirement"

		If above 150, display "Passed the minimum height requirement"

		**Stretch Goal:
			Put inside a funciton
*/

function printheight(height){

	if(height < 150){
		console.log("Did not pass the minimum height requirement");
	} else {
		console.log("Passed the minimum height requirement");
	}
}

printheight(180);

// if, else if, and else statement with Function

let message = "No Message";
console.log(message);

function determineTyphoonIntensity(windSpeed){

	if(windSpeed < 30){
		return "Not a typhoon yet";
	} else if (windSpeed <= 61){
		return "Tropical depression detected";
	} else if (windSpeed >= 62 && windSpeed <= 88){
		return "Tropical storm detected";
	} else if (windSpeed >= 89 && windSpeed <= 117){
		return "Severe tropical storm detected";
	} else {
		return "Typhoon detected";
	}

}

message = determineTyphoonIntensity(70);
console.log(message);

/*
	AND
	all true
*/

// let windSpeed = prompt("Enter the windSpeed:");
// message = determineTyphoonIntensity(windSpeed);
// console.log(message);


if (message === "Tropical storm detected"){
	console.warn(message);
}

// Truthy and Falsy 
/*
	- In Javascript a "truthy" value is a value considered true when encountered in a Boolean context
	- Values are true unless defined otherwise
	- Falsy values/exceptions for truthy 
	1. false
	2. 0
	3. -0
	4. ""
	5. null
	6. undefined
	7. NaN
*/

// Truthy Examples 
if(true){
	console.log("Truthy!");
}

if(1){
	console.log("Truthy!");
}

if([]){
	console.log("Truthy!");
}

// Falsy Examples
if(false){
	console.log("Falsy!");
}

if(0){
	console.log("Falsy!");
}

if(undefined){
	console.log("Falsy!");
}

// Conditional (Ternary) Operator
	/*
		The conditional(ternary) operator takes in 3 operands:
		1. Condition 
		2. Expression to execute if condition is truthy 
		3. Expression to execute if condition is falsy

	- Can be used as alternative to an "if else" statement
	- Ternary operators have an implicit 
	"return" statement. Meaning without "return" keyword, resulting expressions can be stored in a variable 

	Syntax:
		(expression) ? ifTrue: ifFalse;
	*/

// Single Statement Execution
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of the Ternary Operator: " + ternaryResult);

// let ternaryResult = (1 < 18) ? "true" : "false";
// console.log("Result of the Ternary Operator: " + ternaryResult);

// Multiple Statement Execution
let name = prompt("Enter your name");

function isOfLegalAge(){
	// name = "John";
	return "You are of legal age limit";
}

function isUnderAge(){
	// name = "Jane";
	return "You are under the age limit";
}

// "parseInt" function converts input received into a number data type
let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of the Ternary Operator in Functions: " + legalAge + ", " + name);

// Switch Statement 
/*
	- Can be used as alternative to if-else statement where data used in the condition is of an EXPECTED input
	- Almost never used

	Syntax:
		switch (expression){
			case <value>:
				statement;
				break;

			default:
				statement;
				break;
		}
*/

// let day = prompt("What day of the week is it today?").toLowerCase();
// console.log(day);

// switch(day){
// 	case "monday":
// 		console.log("The color of the day is red");
// 		break;
// 	case "tuesday":
// 		console.log("The color of the day is orange");
// 		break;
// 	case "wednesday":
// 		console.log("The color of the day is yellow");
// 		break;
// 	case "thursday":
// 		console.log("The color of the day is green");
// 		break;
// 	case "friday":
// 		console.log("The color of the day is blue");
// 		break;
// 	case "saturday":
// 		console.log("The color of the day is indigo");
// 		break;
// 	case "sunday":
// 		console.log("The color of the day is violet");
// 		break;
// 	default:
// 		console.log("Please input a valid day!");
// 		break;

// }

// Try-Catch-Finally
	/*
		try catch are commonly used for error handling  	
	*/

function showIntensityAlert(windSpeed){
	try{
		//attempt to execute code
		alerat(determineTyphoonIntensity(windSpeed));
	}

	catch (error){
		//catch the error in try statement
		//"error" is user defined. container for error of try
		console.warn(error);
	}

	finally{
		//will be executed regardless of try and catch
		alert("Intensity updates will show new alert");
	}
}

showIntensityAlert(56);